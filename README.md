## Ethereum Bugout Bot

Bugout bot is an Ethereum based trading bot that uses decentralized exchanges to make trades. Its easy to run and can be configured to leverage different risk thresholds and tolerances.

#### bugout bot logic

As Ethereum rises in value, bot will move a configurable percentage of your bankroll into stable and/or interest earning DeFi coins. A portion of these stable profits can be divided up into a series of 'buy back' trades that the bot will use to purchase ETH on the dips. Overall, the bot aims to be on the lower risk side and is based on the idea Ether will gradually rise over the course of the next few years. However, the configuration can be adjusted to take on more risk as desired.

- Developed with Python 3.7.2

- Using the awesome [CoinGecko API](https://www.coingecko.com/en/api) for price data

- Using most excellent DEX aggregator [1inch Exchange](https://1inch.exchange/) to make trades

#### Install

`git clone git@gitlab.com:dentino/ethereum-bugout-bot.git`

`cd ethereum-bugout-bot`

While it's recommend to run in Python virtual environment, it's not required

`python3 -m venv /pyenv/`

`. /pyenv/bin/activate`

#### Setup

**required envars:**

You will need to set and Ethereum provider. This example uses Infura but you can use any node/provider you like

`export ETH_PROVIDER_URL="https://mainnet.infura.io/v3/yourkeygoeshere"`

This is the account that will be used to make trades.

`export BASE_ACCOUNT="0x00000000000000000000000000000"`

Private key for the BASE_ACCOUNT is needed to sign transactions

`export PRIVATE_KEY="base_account_private_key_goes_here"`

**optional envars:**

Set this to force the bot to start strategy with the defined base price. Otherwise, the bot will use the current ETH price when it starts.

`export BASE_ETH_PRICE="150.42"`

#### Configuration Variables

You can adjust these parameters to tune the strategy. **Please adjust with care as they will directly impact how and when the bot buys and sells coins!**

`production_mode = True` - Bot will not attempt to make trades unless this is True

`sell_base_trigger = 4` - Key component of the bot trade strategy. If the prices rises sell_base_trigger percent above the base_price, it will trigger an ETH --> DAI trade. This parameter is also used to construct the buy back strategy and the ride_down_wave.

`ride_down_wave = True` - Should we reset the base_eth_price if we drop down sell_base_trigger percent? default is True. More risky with True but also much less boring.

`portion_of_bankroll = 2` - What percent of our bankroll should we trade when the sell_base_trigger is reached? 2% is taken from common sport betting strategy but you can go up or down to your liking.

`slippage = 1` - 1 Inch slippage

`frequency = 1` - How often does the main bot loop run? In minutes. All testing has been done with at 1 Min.

`buy_back_size = 50` - After sell_base_trigger is reached and ETH-->DAI trade is made, a set of buy back trades can be automated. This parameter denotes how much of that original trade should be used to buy back ETH, should the price drop enough to trigger the buy backs. 50 and less is considered safer, less rewarding. It all depends on the buy back parameters really though.

`total_bankroll = 0` - This has not been implemented yet. Once it has, this will allow you to run the bot with a smaller portion (think hot wallet) of your total bankroll. For example, if you want to run the bot with 1000 ETH. You wouldn't want 1K Eth sitting in bot wallet for safety reasons.
